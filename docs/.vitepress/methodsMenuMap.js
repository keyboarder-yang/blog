// methods 代表类别
// methods：sidebarName 表示该类别在侧边栏显示的名称
// methods：label 表示该类别的子类在侧边栏显示的名称
// methods：name 表示md文档的名称
const methods = {
    'JavaScript': {
        sidebarName: '',
        label: ['测试'],
        name: ['index'],
    },
}
export default methods
