---
layout: home
title: 个人前端笔记
hero:
  name: keyboarder-yang's blog
  tagline: 学问之道无他,求其放心而已矣.
  actions:
    - theme: brand
      text: 开始浏览
      link: /files/
    - theme: alt
      text: GitHub 查看
      link: https://github.com/keyboarder-yang/blog.git

features:
  - title: "个人博客"
    details: 主要用于记录自己在前端开发过程中遇到的问题及解决方法，知识点的总结等。
---
